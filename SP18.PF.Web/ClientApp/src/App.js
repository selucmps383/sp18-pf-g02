import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';

import { Venues } from './components/Venues';
import { Register } from './components/Register';
import { Login } from './components/Login';
import { NotFoundPage } from './components/404NotFound';
import { Events } from './components/Events';
import { Tours } from './components/Tours';
import {Profile} from './components/Profile'
import { Purchase } from './components/Purchase';

import { Test } from './components/Test';


export default class App extends Component {
  displayName = App.name

  render() {
    return (
      <Layout>
            <Route exact path='/' component={Home} />
	        <Route path='/venues' component={Venues} />
	        <Route path='/register' component={Register} />
            <Route path='/login' component={Login} />
            <Route path='/fetchdata' component={FetchData} />
            <Route path='/notfoundpage' component={NotFoundPage} />
            <Route path='/profile' component={Profile} />
            <Route path='/events' component={Events} />
            <Route path='/tours' component={Tours} />
            <Route path='/purchase' component={Purchase} />
      </Layout>
    );
  }
}
