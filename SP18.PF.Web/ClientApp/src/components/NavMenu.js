﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './NavMenu.css';

export class NavMenu extends Component {
  displayName = NavMenu.name
  logout(){
    sessionStorage.setItem("userData", '');
    sessionStorage.clear()
    window.location.reload();
  }
  render() {



    if(sessionStorage.getItem("userData")){
      return (
      <Navbar inverse fixedTop fluid collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={'/'}>Group 8 Entertainment</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to={'/'} exact>
              <NavItem>
                <Glyphicon glyph='home' /> Home
              </NavItem>
            </LinkContainer>
          
            
            
            
            <LinkContainer to={'/venues'}>
              <NavItem>
                <Glyphicon glyph='map-marker' /> Venues
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/tours'}>
              <NavItem>
                <Glyphicon glyph='th-list' /> Tours
              </NavItem>
            </LinkContainer>

            <LinkContainer to={'/profile'}>
                 <NavItem>
                    <Glyphicon glyph='user' /> Profile
                 </NavItem>
             </LinkContainer>

             <NavItem onClick={this.logout}>
                 <Glyphicon glyph='off' /> Logout
             </NavItem>

                      {/*<LinkContainer to={'/Test'}>
              <NavItem>
                <Glyphicon glyph='ban-circle' /> Test
              </NavItem>
            </LinkContainer>*/}


          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }else{
    return (
      <Navbar inverse fixedTop fluid collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={'/'}>Group 8 Entertainment</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to={'/'} exact>
              <NavItem>
                <Glyphicon glyph='home' /> Home
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/Register'}>
              <NavItem>
                <Glyphicon glyph='plus' /> Register
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/Login'}>
              <NavItem>
                <Glyphicon glyph='user' /> Login
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/venues'}>
              <NavItem>
                <Glyphicon glyph='map-marker' /> Venues
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/tours'}>
              <NavItem>
                <Glyphicon glyph='th-list' /> Tours
              </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
    
  }
  }
}
