export function PostUser(type, userData){

    let BaseUrl = 'api/users/register'
    return new Promise((resolve, reject) =>{
      fetch( BaseUrl + type,{
        method: 'POST',
       
        headers: {
          
          'Content-Type': 'application/json',
          
          
      },
        body: JSON.stringify(userData)
      })
      .then((response)=> response.json())
      .then((responseJson) => {
        resolve(responseJson);
      })
      .catch((error) => {
        reject(error);
      });
    });
  }