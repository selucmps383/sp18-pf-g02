import React, { Component } from 'react';


export class FetchData extends Component {
  displayName = FetchData.name

  constructor(props) {
    super(props);
      this.state = { event: [], loading: true };

    
    fetch('api/events')
      .then(response => response.json())
      .then(data => {
        this.setState({ event: data, loading: false });
      });
  }

   
    static renderEventTable(event) {
        let ImagesSrc = [
            "https://static.pexels.com/photos/248963/pexels-photo-248963.jpeg",
            "http://thesocialrush.com/wp-content/uploads/2017/05/image-1.jpg",
            "http://www.cbc.ca/parents/content/imgs/kidsatconcerts_lead_emissio.jpg",
            "http://wvuafm.ua.edu/wp-content/uploads/2013/04/Concert1.jpg",
            "http://www.busestoconcerts.com/communities/8/004/008/554/698//images/4621827369.jpg",
            "https://www.sydneyoperahouse.com/content/dam/soh/visit-us/performance-spaces/concert-hall/CONCERT_HALL_GALLERY_La-Grand-Cirque-opening-night-041%20Dan%20Boud_1600x900.jpg",
            "https://media.timeout.com/images/103161025/image.jpg",
            "http://static.tumblr.com/7b37633d29d9aaca9828b5132273cdf1/sy59fz4/1linmyvvt/tumblr_static_4y2skv97m24ogs84wc40sccso.jpg",
            "https://www.digitalconcerthall.com/gfx/teaser/dch-teaser-background.png",
            "https://www.wien.info/media/images/31709-musikverein-neujahrskonzert-goldener-saal-wiener-philharmoniker-3to2.jpeg/image_gallery"];

        const imgStyle = {
            minHeight: '150px',
            height: '150px',
            objectFit: 'fill',
            backgroundColor: 'grey'
        };

      return (
           
          <div class="row">
              {event.map(event =>
                  <div class="col-sm-6 col-md-4" key={event.id}>
                      <div class="thumbnail ">
                          <img style={imgStyle} src={ImagesSrc[Math.floor(Math.random() * ImagesSrc.length)]}  />
                          <div class="caption">
                              <h3>{event.tourName}</h3>
                              <p>{event.venueName} | <span>Price: <strong>$ {event.ticketPrice}</strong></span></p>
                              <p><strong>Start :</strong> {event.eventStart}</p>
                              <p><strong>End :</strong> {event.eventEnd}</p>
                              <form action="/Events" class="inline">
                                  <button class="float-left submit-button" >See Events</button>
                              </form>
                          </div>
                      </div>

                      </div>  
              )}
              
          </div>
               
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : FetchData.renderEventTable(this.state.event);

    return (
      <div>
        <h1>Tours</h1>
        {contents}
      </div>
    );
  }
}
