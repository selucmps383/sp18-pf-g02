export function PostData(type, userData) {

    let BaseUrl = 'api/users/'
    return new Promise((resolve, reject) => {
        fetch(BaseUrl + type, {
            method: 'POST',
            body: JSON.stringify(userData),
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then((response) => response.json())
            .then((res) => {
                resolve(res);
            })
            .catch((error) => {
                reject(error);
            });
    });
}

export function PostApiData(url, data) {

    console.log(' -- post api data called -- ');
    console.log(arguments);

    const baseUrl = '/api/';
    return fetch(baseUrl + url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then((res) => {
            console.log('post api data', res);
            return res.json();
        });

}