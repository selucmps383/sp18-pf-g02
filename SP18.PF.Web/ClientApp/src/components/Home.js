import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';

export class Home extends Component {
  displayName = Home.name

  constructor(props){
    super(props);
    this.state={
      redirect: false
    }
   
  }

  componentWillMount(){
    if(sessionStorage.getItem("userData")){
      console.log("Call User");
    }
    else{
      this.setState({redirect: false});
    }
  }



  render() {



    if(this.state.redirect){
        return(<Redirect to={'/login'}/>)
      
    }



    return (
      <div>
            <h1>Group 8 Entertainment</h1>
       
      </div>
    );
  }
}
