﻿import React, { Component } from 'react';

export class Purchase extends Component {
    displayName = Purchase.name

    constructor(props) {
        super(props);
        this.state = { ticket: [], loading: true };

        fetch('api/tickets/purchase/8')
            .then(response => response.json())
            .then(data => {
                this.setState({ ticket: data, loading: false });
            });

    }

    static renderTicketTable(ticket) {
        return (

            <div class="row">
                {ticket.map(tickets =>
                    <div class="col-sm-6 col-md-4" key={tickets.id}>
                        <div class="thumbnail">
                            <img src="http://thesocialrush.com/wp-content/uploads/2017/05/image-1.jpg" alt="..." />
                            <div class="caption">
                                <h3>{tickets.id}</h3>
                            </div>
                        </div>
                    </div>
                )}

            </div>

        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading your ticket...</em></p>
            : Purchase.renderEventTable(this.state.ticket);

        return (
            <div>
                <h1>Purchase</h1>
                {contents}
                <form action="/venues" class="inline">
                    <button class="float-left submit-button" >See all Venues</button>
                </form>
                <form action="/tours" class="inline">
                    <button class="float-left submit-button" >See all Tours</button>
                </form>
            </div>
        );
    }
}