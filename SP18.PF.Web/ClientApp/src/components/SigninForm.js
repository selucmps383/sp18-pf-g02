import React from 'react';
import {PostData} from './PostData';
import {Redirect} from 'react-router-dom';

export class SigninForm extends React.Component{

    constructor(props) {
        super(props);
        this.state ={
          email: '',
          password:'',
          redirectToReferrer: false
        }
        this.login = this.login.bind(this);
        this.onChange = this.onChange.bind(this);
      }

      login(){
        if(this.state.email && this.state.password){
        
            PostData('login', this.state)
                .then((result) => {
                    if (typeof result.id === "number") {
                        console.log("user id received, valid login", result);
                        sessionStorage.setItem('userData', JSON.stringify(result));
                        this.setState({ redirectToReferrer: true });
                    } else {
                        console.log("login fail", result);
                        alert("Login failed");
                    }
        });
      }
    }

      onChange(e){
       this.setState({[e.target.name]: e.target.value});
       
       
      }
    
      
      render() {


        if(this.state.redirectToReferrer){
            return (<Redirect to={'/'}/>)
        }

        if(sessionStorage.getItem("userData")){
          return(<Redirect to={'/home'}/>)
        
      }
        return (

        
         
         <div>
           <h1>Login</h1>
        <label>Email</label>
                <input type="text" name="email" placeholder="email" onChange={this.onChange} />

        <label>Password</label>
                <input type="password" name="password" placeholder="password" onChange={this.onChange} />

          <input type="submit" value="Login" className="button" onClick={this.login}/>
         </div>
        );
      }
    }