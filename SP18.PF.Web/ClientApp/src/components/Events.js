﻿import React, { Component } from 'react';

export class Events extends Component {
    displayName = Events.name

    constructor(props) {
        super(props);
        this.state = {
            venue: [],
            loading: true,
        };

        fetch('api/events')
            .then(response => response.json())
            .then(data => {
                this.setState({ event: data, loading: false });
            });
    }

    static renderEventTable(event) {
        return (
            <div class="row">
                {event.map(event =>
                    <div class="col-sm-6 col-md-4" key={event.id}>
                        <div class="thumbnail">
                            <img src="http://thesocialrush.com/wp-content/uploads/2017/05/image-1.jpg" alt="..." />
                            <div class="caption">
                                <h3>{event.id}</h3>
                                <h2>{event.tourName}</h2>
                                <h2>{event.venueName}</h2>
                                <p>{event.eventStart}</p>
                                <p>{event.eventEnd}</p>
                                <form action="/Purchase" class="inline">
                                    <button class="float-left submit-button" >Purchase Now</button>
                                </form>
                           </div>
                        </div>
                    </div>
                )}

            </div>

        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading your events...</em></p>
            : Events.renderEventTable(this.state.event);

        return (
            <div>
                <h1>Events {this.state.venueId}</h1>
                {contents}
                <form action="/venues" class="inline">
                    <button class="float-left submit-button" >See all Venues</button>
                </form>
                <form action="/tours" class="inline">
                    <button class="float-left submit-button" >See all Tours</button>
                </form>
            </div>
        );
    }
}