import React, { Component } from 'react';

export class Venues extends Component {
    displayName = Venues.name

    constructor(props) {
        super(props);
        this.state = { venue: [], loading: true };

        fetch('api/venues')
            .then(response => response.json())
            .then(data => {
                this.setState({ venue: data, loading: false });
            });
    }

    static renderVenueTable(venue) {
        return (

            <div class="row">
                {venue.map(venue =>
                    <div class="col-sm-6 col-md-4" key={venue.id}>
                        <div class="thumbnail">
                            <img src="http://thesocialrush.com/wp-content/uploads/2017/05/image-1.jpg" alt="..." />
                            <div class="caption">
                                <h3>{venue.name}</h3>
                                <p>{venue.description}</p>
                                <p>{venue.physicalAddress.addressLine1} {venue.physicalAddress.addressLine2}</p>
                                <p>{venue.physicalAddress.city}, {venue.physicalAddress.state} {venue.physicalAddress.zipCode}</p>
                                <form action="/Events" class="inline">
                                    <button class="float-left submit-button" >See Events</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    )
                }

            </div>

        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : Venues.renderVenueTable(this.state.venue);

        return (
            <div>
                <h1>Venue</h1>
                {contents}
            </div>
        );
    }
}