﻿import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

export class Profile extends Component {
    displayName = Profile.name

    cachedData = sessionStorage.getItem("userData")
    

    render() {
        console.log(this.cachedData);
        const userDetails = JSON.parse(this.cachedData);
        let email = userDetails.email;

        fetch('/api/users/user-details/?email=' + email, {
            method: 'GET'
        }).then((res) => {
            console.log("res=", res);
        })

            .catch((err) => { console.log(err); });


        return (
            <div>
                <div className="content">
                    <h1>Users Profile</h1>
                    <p><strong>Email:</strong> {userDetails.email}</p>
                    <p><strong>Role:</strong> {userDetails.role}</p>
                </div>
            </div>
        );
    }
}
