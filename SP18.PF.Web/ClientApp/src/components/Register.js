import React, {Component} from 'react';
import {SignupForm} from './SignupForm';


export class Register extends Component{
    render(){
        return(
           
          <div register="row">
            <div register="col-md-4 col-md-offset-4">
                <SignupForm/>
            </div>
            </div>
        );
    }
}

