﻿import React, { Component } from 'react';
import { PostData } from './PostData';

export class Test extends Component {
    displayName = Test.name

    constructor(props) {
        super(props);
        this.state = {
            venue: [],
            loading: true,
            eventId: 0
        };

        fetch('api/events')
            .then(response => response.json())
            .then(data => {
                this.setState({ event: data, loading: false });
            });
    }

    purchase() {
        if (this.state.eventId) {

            PostData('purchase', this.state).then((result) => {
                let responseJSON = result;
                if (responseJSON.userData) {

                } else {
                    sessionStorage.setItem('userData', JSON.stringify(responseJSON));
                    this.setState({ redirectToReferrer: true });

                }
            });
        }
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });


    }

    static renderEventTable(event) {
        return (
            <div class="row">
                {event.map(event =>
                    <div class="col-sm-6 col-md-4" key={event.id}>
                        <div class="thumbnail">
                            <img src="http://thesocialrush.com/wp-content/uploads/2017/05/image-1.jpg" alt="..." />
                            <div class="caption">
                                <h3>{event.id}</h3>
                                <h3>${event.ticketPrice}</h3>
                                <h2>{event.tourName}</h2>
                                <h2>{event.venueName}</h2>
                                <p>{event.eventStart}</p>
                                <p>{event.eventEnd}</p>
                                <input type="submit" value="Purchase Now" className="button" onClick={this.purchase} />
                            </div>
                        </div>
                    </div>
                )}

            </div>

        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading your events...</em></p>
            : Test.renderEventTable(this.state.event);

        return (
            <div>
                <h1>Events {this.state.venueId}</h1>
                {contents}
                <form action="/venues" class="inline">
                    <button class="float-left submit-button" >See all Venues</button>
                </form>
                <form action="/tours" class="inline">
                    <button class="float-left submit-button" >See all Tours</button>
                </form>
            </div>
        );
    }
}