﻿import React, { Component } from 'react';


export class Tours extends Component {
    displayName = Tours.name

    constructor(props) {
        super(props);
        this.state = { tour: [], loading: true };


        fetch('api/tours')
            .then(response => response.json())
            .then(data => {
                this.setState({ tour: data, loading: false });
            });
    }


    static renderTourTable(tour) {
        let ImagesSrc = [
            "https://static.pexels.com/photos/248963/pexels-photo-248963.jpeg",
            "http://thesocialrush.com/wp-content/uploads/2017/05/image-1.jpg",
            "http://www.cbc.ca/parents/content/imgs/kidsatconcerts_lead_emissio.jpg",
            "http://wvuafm.ua.edu/wp-content/uploads/2013/04/Concert1.jpg",
            "http://www.busestoconcerts.com/communities/8/004/008/554/698//images/4621827369.jpg",
            "https://www.sydneyoperahouse.com/content/dam/soh/visit-us/performance-spaces/concert-hall/CONCERT_HALL_GALLERY_La-Grand-Cirque-opening-night-041%20Dan%20Boud_1600x900.jpg",
            "https://media.timeout.com/images/103161025/image.jpg",
            "http://static.tumblr.com/7b37633d29d9aaca9828b5132273cdf1/sy59fz4/1linmyvvt/tumblr_static_4y2skv97m24ogs84wc40sccso.jpg",
            "https://www.digitalconcerthall.com/gfx/teaser/dch-teaser-background.png",
            "https://www.wien.info/media/images/31709-musikverein-neujahrskonzert-goldener-saal-wiener-philharmoniker-3to2.jpeg/image_gallery"];

        const imgStyle = {
            minHeight: '150px',
            height: '150px',
            objectFit: 'fill',
            backgroundColor: 'grey'
        };

        return (

            < div class="row" >
                {tour.map(tour =>
                    <div class="col-sm-6 col-md-4" key={tour.id}>
                        <div class="thumbnail ">
                            <img style={imgStyle} src={ImagesSrc[Math.floor(Math.random() * ImagesSrc.length)]} />
                            <div class="caption">
                                <h3>{tour.name}</h3>
                                <p>{tour.description}</p>
                                <form action="/Events" class="inline">
                                    <button type="Submit" value={tour.id} class="float-left submit-button" >See Events</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    )
                }
            </div >

        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : Tours.renderTourTable(this.state.tour);

        return (
            <div>
                <h1>Tours</h1>
                {contents}
            </div>
        );
    }
}
