import React from 'react';
import InputMask from 'react-input-mask';
import './SignUpForm.css';
import { PostData, PostApiData } from './PostData';
import { Redirect } from 'react-router-dom';
import { log } from 'util';

export class SignupForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            addressLine1: '',
            addressLine2: '',
            zipCode: '',
            city: '',
            state: ''
            
           
        }
        this.onChange = this.onChange.bind(this);
        this.signup = this.signup.bind(this);
    }

    signup(event) {
        console.log("signup", this);
        event.preventDefault();

        

        var postData = {
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword,
            billingAddress: {
                addressLine1: this.state.addressLine1,
                addressLine2: this.state.addressLine2,
                city: this.state.city,
                state: this.state.state,
                zipCode: this.state.zipCode

            }
        }

        console.log(' post data : ', postData);

        PostApiData('users/register', postData).then((result) => {
           let responseJson = result;
           if (typeof responseJson.id === "number") {
                sessionStorage.setItem('userData', JSON.stringify(responseJson));
                this.setState({ redirectToReferrer: true });
            }
        });

    }

    onChange(e) {
        //console.log("on change called", e.target.name, e.target.value, e.target);
        var key = e.target.name;
        var value = e.target.value;
        var obj = {};
        obj[key] = value;
        this.setState(obj);
    }

    render() {


        if (this.state.redirectToReferrer || sessionStorage.getItem('userData')) {
            return (<Redirect to={'/'} />)
        }

        return (

            <div className="col-md-10">
                <form onSubmit={this.signup} className="form">
                    <div id="regtitle">
                        <h1>Register</h1>
                    </div>

                    <div className="form-group">
                        <label htmlFor="email" className="col-md-3 control-label">Email</label>
                        <div className="col-md-9">
                            <input value={this.state.email} onChange={this.onChange} placeholder="email" type="text" name="email" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password" className="col-md-3 control-label">Password</label>
                        <div className="col-md-9">
                            <input value={this.state.password} onChange={this.onChange} placeholder="password" type="password" name="password" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="confirmPassowrd" className="col-md-3 control-label">Confirm Password</label>
                        <div className="col-md-9">
                            <input value={this.state.confirmPassword} onChange={this.onChange} placeholder="confirmPassword" type="password" name="confirmPassword" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="addressLine1" className="col-md-3 control-label">Address Line 1</label>
                        <div className="col-md-9">
                            <input value={this.state.addressLine1} onChange={this.onChange} placeholder="addressLine1" type="text" name="addressLine1" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="addressLine2" className="col-md-3 control-label">Address Line 2</label>
                        <div className="col-md-9">
                            <input value={this.state.addressLine2} onChange={this.onChange} placeholder="addressLine2" type="text" name="addressLine2" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="city" className="col-md-3 control-label">City</label>
                        <div className="col-md-9">
                            <input value={this.state.city} onChange={this.onChange} placeholder="city" type="text" name="city" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="state" className="col-md-3 control-label">State</label>
                        <div className="col-md-9">
                            <InputMask value={this.state.state} onChange={this.onChange} mask="aa" maskChar=" " placeholder="Enter state eg. LA, FL, NE" name="state" />

                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="zipcode" className="col-md-3 control-label">Zip Code</label>
                        <div className="col-md-9">
                            <InputMask value={this.state.zipcode} onChange={this.onChange} mask="99999" placeholder="Enter your zipcode." maskChar=" " name="zipCode" />
                        </div>
                    </div>
                    <button className="btn btn-success" type="submit">Register</button>
                </form>

            </div>
        );
    }
}