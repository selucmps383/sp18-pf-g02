﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Auth;

namespace SP18.PF.Forms
{
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Logout(object sender, EventArgs e)
        {
            var uri = new Uri(Appsettings.ApiUrl + "api/logout");
            Device.OpenUri(uri);
        }

        private void SendEmail(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("mailto:383@envoc.com"));
        }

        private void AlabamaButton(object sender, EventArgs e)
        {
            var uri = new Uri("https://www.google.com/maps/dir/Southeastern+Louisiana+University,+West+University+Avenue,+Hammond,+LA/Alabama+Theatre,+1817+3rd+Ave+N,+Birmingham,+AL+35203/@31.8929135,-90.9191876,7z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x862722985b04e61d:0xd65f00282c35660!2m2!1d-90.4688572!2d30.5173159!1m5!1m1!1s0x88891b93d9c2fb7b:0x607c0735ce1f64d3!2m2!1d-86.8090934!2d33.5148887");
            Device.OpenUri(uri);
        }


        private void BJCCButton(object sender, EventArgs e)
        {
            var uri = new Uri("https://www.google.com/maps/dir/Southeastern+Louisiana+University,+West+University+Avenue,+Hammond,+LA/BJCC+Theatre,+19th+Street+North,+Birmingham,+AL/@31.8985568,-90.9185496,7z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x862722985b04e61d:0xd65f00282c35660!2m2!1d-90.4688572!2d30.5173159!1m5!1m1!1s0x88891b8494d4878d:0x4379f4c662cb4391!2m2!1d-86.8134216!2d33.5249829");
            Device.OpenUri(uri);
        }


        private void CarlsonButton(object sender, EventArgs e)
        {
            var uri = new Uri("https://www.google.com/maps/dir/Southeastern+Louisiana+University,+West+University+Avenue,+Hammond,+LA/Carlson+Theater,+Landerholm+Circle+Southeast,+Bellevue,+WA/@38.1575063,-122.9914068,4z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x862722985b04e61d:0xd65f00282c35660!2m2!1d-90.4688572!2d30.5173159!1m5!1m1!1s0x54906ea0b00fcab7:0x1be09411a843e786!2m2!1d-122.1496194!2d47.5833703");
            Device.OpenUri(uri);
        }


        private void ComericaButton(object sender, EventArgs e)
        {
            var uri = new Uri("https://www.google.com/maps/dir/Southeastern+Louisiana+University,+West+University+Avenue,+Hammond,+LA/Comerica+Theatre,+West+Washington+Street,+Phoenix,+AZ/@30.1707778,-119.2995969,4z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x862722985b04e61d:0xd65f00282c35660!2m2!1d-90.4688572!2d30.5173159!1m5!1m1!1s0x872b12243acc9d71:0x68e0b6c3c86eaa97!2m2!1d-112.0793785!2d33.4489148");
            Device.OpenUri(uri);
        }


        private void WarButton(object sender, EventArgs e)
        {
            var uri = new Uri("https://www.google.com/maps/dir/Southeastern+Louisiana+University,+West+University+Avenue,+Hammond,+LA/War+Memorial+Stadium,+Stadium+Dr,+Little+Rock,+AR/@32.6143059,-93.5964117,7z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x862722985b04e61d:0xd65f00282c35660!2m2!1d-90.4688572!2d30.5173159!1m5!1m1!1s0x87d2a49d659585a9:0x6397a42fdde88983!2m2!1d-92.3301268!2d34.749861");
            Device.OpenUri(uri);
        }


        private void AngelButton(object sender, EventArgs e)
        {
            var uri = new Uri("https://www.google.com/maps/dir/Southeastern+Louisiana+University,+West+University+Avenue,+Hammond,+LA/Angel+Stadium+Parking,+Orangewood+Ave,+Anaheim,+CA/@30.4928707,-122.1974704,4z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x862722985b04e61d:0xd65f00282c35660!2m2!1d-90.4688572!2d30.5173159!1m5!1m1!1s0x80dcd783713da0d3:0xdc3504c823fcf298!2m2!1d-117.8829139!2d33.7953722");
            Device.OpenUri(uri);
        }


        private void HouseButton(object sender, EventArgs e)
        {
            var uri = new Uri("https://www.google.com/maps/dir/Southeastern+Louisiana+University,+West+University+Avenue,+Hammond,+LA/House+of+Blues+New+Orleans,+Decatur+Street,+New+Orleans,+LA/@30.232427,-90.5658267,10z/data=!3m2!4b1!5s0x8620a60d7382e8d7:0x93ba42defb2aed90!4m13!4m12!1m5!1m1!1s0x862722985b04e61d:0xd65f00282c35660!2m2!1d-90.4688572!2d30.5173159!1m5!1m1!1s0x8620a60d70877d4d:0x586ea62622b34a6d!2m2!1d-90.066177!2d29.953354");
            Device.OpenUri(uri);
        }


        private void HollyButton(object sender, EventArgs e)
        {
            var uri = new Uri("https://www.google.com/maps/dir/Southeastern+Louisiana+University,+West+University+Avenue,+Hammond,+LA/Hollywood+Bowl+Rd,+Los+Angeles,+CA+90068/@30.4937325,-122.4227776,4z/data=!3m2!4b1!5s0x8620a60d7382e8d7:0x93ba42defb2aed90!4m13!4m12!1m5!1m1!1s0x862722985b04e61d:0xd65f00282c35660!2m2!1d-90.4688572!2d30.5173159!1m5!1m1!1s0x80c2bf1a2ff8c619:0xdb3eb217fb84b39e!2m2!1d-118.3367698!2d34.1119522");
            Device.OpenUri(uri);
        }


        private void GreekButton(object sender, EventArgs e)
        {
            var uri = new Uri("https://www.google.com/maps/dir/Southeastern+Louisiana+University,+West+University+Avenue,+Hammond,+LA/The+Greek+Theatre,+North+Vermont+Avenue,+Los+Angeles,+CA/@30.498173,-122.4011187,4z/data=!3m2!4b1!5s0x8620a60d7382e8d7:0x93ba42defb2aed90!4m13!4m12!1m5!1m1!1s0x862722985b04e61d:0xd65f00282c35660!2m2!1d-90.4688572!2d30.5173159!1m5!1m1!1s0x80c2c098a47d4245:0x3b9f3214123aa486!2m2!1d-118.2960682!2d34.1197351");
            Device.OpenUri(uri);
        }


        private void MicrosoftButton(object sender, EventArgs e)
        {
            var uri = new Uri("https://www.google.com/maps/dir/Southeastern+Louisiana+University,+West+University+Avenue,+Hammond,+LA/Microsoft+Theater,+Chick+Hearn+Court,+Los+Angeles,+CA/@30.4927728,-122.389438,4z/data=!3m2!4b1!5s0x8620a60d7382e8d7:0x93ba42defb2aed90!4m13!4m12!1m5!1m1!1s0x862722985b04e61d:0xd65f00282c35660!2m2!1d-90.4688572!2d30.5173159!1m5!1m1!1s0x80c2c7b900ec5d7f:0xc71d771bd9c0cf0f!2m2!1d-118.267087!2d34.044403");
            Device.OpenUri(uri);
        }
    }
}
