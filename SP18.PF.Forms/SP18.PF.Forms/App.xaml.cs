﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace SP18.PF.Forms
{
	public partial class App : Application
	{    
        public App ()
		{
			InitializeComponent();

            var accountService = new AccountService();

            if (string.IsNullOrEmpty(accountService.UserName))
            {
                MainPage = new SP18.PF.Forms.LoginPage();
            }
			else
            {
                MainPage = new SP18.PF.Forms.MainPage();
            }
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
