﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SP18.PF.Forms.Models
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public ICommand LoginCommand { get; set; }

        public LoginModel()
        {
            LoginCommand = new Command(OnSubmit);
        }

        public void OnSubmit()
        {
            
            if (string.IsNullOrEmpty(Email))
            {
                MessagingCenter.Send(this, "LoginAlert", Email);
            }
        }
    }
}
