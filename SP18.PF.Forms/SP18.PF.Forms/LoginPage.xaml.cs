﻿using SP18.PF.Forms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SP18.PF.Forms.Models;
using Newtonsoft.Json;

namespace SP18.PF.Forms
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
        //public LoginModel loginModel;
		public LoginPage ()
		{
			InitializeComponent ();
            //loginModel = new LoginModel();
            
            //this.BindingContext = loginModel;
		}
        public class User
        {
            public string Email { get; internal set; }
            public string Password { get; internal set; }
        }

        private async void SignIn(object sender, EventArgs e)
        {
            
            
            User user = new User();
            user.Email = Entry_Email.Text;
            user.Password = Entry_Password.Text;
            string output = JsonConvert.SerializeObject(user);
            var baseAddress = new Uri(Appsettings.ApiUrl);
            var cookieContainer = new CookieContainer();
            var handler = new HttpClientHandler
            {
                CookieContainer = cookieContainer
            };

            var client = new HttpClient(handler)
            {
                BaseAddress = baseAddress
            };
            HttpContent content = new StringContent(output, Encoding.UTF8, "application/json");    
            var result = await client.PostAsync("users/login", content);
            cookieContainer.Add(baseAddress, new Cookie("email", Entry_Email.Text));
            
            CookieCollection collection = handler.CookieContainer.GetCookies(baseAddress);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                AccountService accountService = new AccountService();
                accountService.SaveCredentials(Entry_Email.Text, Entry_Password.Text);
            }
        }
    }
}